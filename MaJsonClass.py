'''
Created on 15 août 2018

@author: Marc <framagit@framboisier.net>
'''

class MaJsonClass(object):
    '''
    classdocs
    '''


    def __init__(self, **attributes):
        '''
        Constructor
        '''
        for nomAttribut, valeurAttribut in attributes.items():
            setattr(self,nomAttribut, valeurAttribut)
