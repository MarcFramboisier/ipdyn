'''
Created on 15 août 2018

@author: Marc <framagit@framboisier.net>
'''
import json
import requests
from requests.exceptions import RequestException
from MaJsonClass import MaJsonClass

class GandiConnectError(Exception):
    def __init__(self, msg):
        self.message=msg
        
class GandiHTTPError(Exception):
    def __init__ (self,statusCode,msg):
        self.statusCode=statusCode
        self.message=msg

class GandiConnect(object):
    '''
    Les manipulations avec l'API de Gandi
    '''


    def __init__(self, apiKey,zone,gandiUrl='https://dns.api.gandi.net/api/v5/'):
        '''
        Constructor
        '''
        self.apiKey=apiKey
        self.baseUrl=gandiUrl
        self.zoneId=''
        self.zoneId= self.getZoneId(zone)
        
    def gandiReq(self, reqType,requete, entete={}, data=None):
        headers={'X-Api-Key' : self.apiKey}
        if reqType in ('put','post'):
            headers.update({'Content-Type' : 'application/json'})
        headers.update(entete)
        requete = self.baseUrl + requete
        try:
            if reqType=='delete':
                r = requests.delete(requete, headers=headers)
            elif reqType=='put':
                r = requests.put(requete, headers=headers, data=data)
            elif reqType=='post':
                r = requests.post(requete, headers=headers, data=data)
            else:
                r = requests.get(requete, headers=headers)
        except RequestException as e:
            msg = 'Erreur dans la requete ' + reqType + ' : ' + requete + ' : '+ str(e)
            raise GandiConnectError(msg)
            return
        if r.status_code >= 400 :
            msg= 'Erreur dans la requete ' + reqType + ' : ' + requete + ' : ' +r.text
            raise GandiHTTPError(r.status_code, msg)
            return
        try :
            resp = r.json()
        except Exception as e :
            msg = 'JSON erreur : ' + r.text
            raise GandiConnectError(msg)
            return
        
        return resp
                
                
        
        print(headers)
        return
    
    def getZoneId(self,zone):
        zones = self.gandiReq('get','zones')
        for zoneDict in zones :
            z = MaJsonClass(**zoneDict)
            if (z.name == zone):
                self.zoneId=z.uuid
        if self.zoneId == '' :
            raise GandiConnectError('zone : ' + zone + ' inexistante')
            return
        return self.zoneId
    
    def getEnregistrement(self,label,typeEnregistrement):
        if self.zoneId=='' :
            raise GandiConnect('Erreur lecture enregistrement : uuid vide (incident à l\'intialisation')
            return
        try : 
            enreg=self.gandiReq('get', 'zones/'+self.zoneId+'/records/'+label+'/'+typeEnregistrement)
        except GandiHTTPError as e:
            if e.statusCode == 404 :
                return ''
            else:
                raise GandiConnectError(e.message)
                return
        except GandiConnectError as e :
            raise GandiConnectError(e.message)
            
        return enreg['rrset_values'][0]

    def setEnregistrement(self,label,typeEregistrement,valeur,ttl):
        data = json.dumps({'rrset_ttl' : ttl , 'rrset_values' : [valeur] })
        requete = '/zones/' + self.zoneId + '/records/' + label + '/'+typeEregistrement
        try:
            resp = self.gandiReq('put',requete,data=data)
        except Exception as e:
            raise GandiConnectError(e.message)
            return
        return resp
        
        
        
        
        
        