# ipDyn
IpDyn met à jour un enregistrement DNS chez Gandi avec les adresses IPv4 et IPv6 courantes de la machine sur laquelle il est exécuté.
Mise à jour du 20 Mai 2022, réupération des adresses ip au format json


[Pour plus d'information](https://www.framboisier.com/blog/2018/08/18/dyn-dns-gandi-live/)
