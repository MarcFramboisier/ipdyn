'''
Created on 13 août 2018
Modif 20 Mai 2022 adapation json

@author: Marc <framagit@framboisier.net>
'''

import requests
import json

class NoIP(Exception):
    def __init__(self, errors, code=None):
        self.errors=errors
        self.code=code

class GetIP:
    def __init__(self,urlv4,urlv6):
        self.urlv4=urlv4
        self.urlv6=urlv6
        pass
    
    


    def _getIP(self,version):
        s=None
        if version == 'v4':
            url = self.urlv4
        elif version=='v6':
            url = self.urlv6
        else:
            return s
        try:
            r = requests.get(url)
        except requests.exceptions.RequestException as e:
            msg = 'GetIP failled : '+ str(e)
            raise NoIP(msg)
            return s
        page=r.text
        r.close()
        try:
	        data=json.loads(page)
        except Exception as e:
            msg = 'GetIP failled : '+ str(e)
            raise NoIP(msg)
            return s
        try:
            res=data['ipaddress']
        except Exception as e:
            msg = 'GetIP failled : '+ str(e)
            raise NoIP(msg)
            return s
        if res== None:
            raise NoIP('adresse IP non trouvée sur la page')
            return s
        s = res
        return s
    
    def getIPv4(self):
        return self._getIP('v4')
    
    def getIPv6(self):
        return self._getIP('v6')
    
