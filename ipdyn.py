#!/usr/bin/python3
#coding : utf-8
'''
Created on 14 août 2018

@author: Marc <framagit@framboisier.net>
Licence GNU GPLv3
'''
import argparse
from ipdynParam import IpdynParam
from ipdynParam import ConfigError
from gandiconnect import GandiConnect, GandiConnectError
from GetIP import GetIP
from GetIP import NoIP
from datetime import datetime



def main (fichierIni):
    print('-----------------------------------------------')
    print('Lancement : ' + datetime.isoformat(datetime.now()))

    try:
        maConf=IpdynParam(fichierIni)
    except ConfigError as e :
        print(e.message)
        return
    print('domaine : ' + maConf.domain + '  Enregistrement : ' + maConf.name)
    
    ipv4Flag=False #Pour dire de traiter IPv4
    ipv6Flag=False #Pour dire de traiter IPv6
    
    
    #récupération des @IPv4 et IPv6 courante.
    getIP = GetIP(maConf.urlIpv4, maConf.urlIpv6)
    ipv4=''
    if maConf.urlIpv4 != '':
        try:
            ipv4 = getIP.getIPv4()
        except NoIP as e:
            print('erreur pour récupérer IPv4 :' + str(e))
        else :
            ipv4Flag = True
    ipv6=''
    if maConf.urlIpv6 != '':
        try:
            ipv6 = getIP.getIPv6()
        except NoIP as e:
            print('erreur pour récupérer IPv6 :' + str(e))
        else :
            ipv6Flag = True
    
    print('IP actuelles ' + ipv4 + ' : ' + ipv6)        
    #L'objet perso pour se connecter à l'API de Gandi
    gc = GandiConnect(maConf.apiKey , maConf.domain, maConf.urlGandi)
    
    
    # on traite IPv4
    
    if ipv4Flag:
        for label in maConf.names :
            print('traitement IPv4 '+label+'.'+maConf.domain)
            try :
                ipv4DNS = gc.getEnregistrement(label, 'A')
            except GandiConnectError as e :
                print('erreur lecture IPv4 chez Gandi : '+e.message)
            else :
                print('IPv4 chez Gandi : ' + ipv4DNS)
                if ipv4 != ipv4DNS :
                    try :
                        gc.setEnregistrement(label, 'A', ipv4, maConf.ttl)
                    except GandiConnectError as e :
                        print('erreur écriture IPv4 chez Gandi : ' + e.message)
                    else :
                        print('IPv4 changée')
                else :
                    print('pas de changement ipV4 nécessaire')
    # on traite IPv4
    if ipv6Flag:
        for label in maConf.names :
            print('traitement IPv6 '+label+'.'+maConf.domain)
            try :
                ipv6DNS = gc.getEnregistrement(label, 'AAAA')
            except GandiConnectError as e :
                print('erreur lecture IPv6 chez Gandi : '+e.message)
            else :
                print('IPv6 chez Gandi : ' + ipv6DNS)
                if ipv6 != ipv6DNS :
                    try :
                        gc.setEnregistrement(label, 'AAAA', ipv6, maConf.ttl)
                    except GandiConnectError as e :
                        print('erreur écriture IPv6 chez Gandi : ' + e.message)
                    else :
                        print('IPv6 changée')
                else :
                    print('pas de changement ipV6 nécessaire')
    
    
    print('fin : ' + datetime.isoformat(datetime.now()))
    print('-----------------------------------------------')
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Met à jour un enregistrement chez Gandi avec les @IP de la machine')
    parser.add_argument('fichier', type=str, help='fichier de paramètres')
    args = parser.parse_args()
    main(args.fichier)
  
    