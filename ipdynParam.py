'''
Created on 14 août 2018

@author: Marc <framagit@framboisier.net>
'''

import configparser

class ConfigError(Exception):
    def __init__(self, msg):
        self.message=msg

class IpdynParam(object):
    '''
    classdocs
    '''


    def __init__(self, ficIni):
        '''
        Constructor
        '''
        config = configparser.ConfigParser()
        try:
            config.read(ficIni)
        except Exception as e:
            raise ConfigError(str(e))
        args = {'domain' : True,
                'name' : True,
                'ttl': True,
                'apiKey' : True,
                'urlGandi' : True,
                'urlIpv4' : False,
                'urlIpv6' : False }
        
        
        for a , obligatoire in args.items() :
            try:
                attVal = config['Main'][a]
                setattr(self,a, attVal)
            except KeyError as e :
                msg = 'valeur manquante dans fichier de configuration ' + str(e)
                raise ConfigError(msg)
                return
            if attVal=='' and obligatoire :
                msg = 'Valeur vide dans le fichier de configuration ' + a
                raise ConfigError(msg)
                return
        self.names = self.name.split(';')
        print(self.names)
        
        